#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "no parameter...";
    exit 1;
fi

HERE="$(pwd)"
BUILDS="$HERE/builds"
DIR="$BUILDS/$1"

rm -rf $DIR;
mkdir $DIR;

echo "Make-ing dsdaemon..."
cd ../dsdaemon/
./make release $DIR

echo "threshold-provider..."
cd ../threshold-provider/
./make release
cp threshold-provider $DIR/;
cp config.json $DIR/threshold-provider-example-config.json

echo "utilization-provider..."
cd ../utilization-provider/
./make release
cp bin/* $DIR/

# RABBITMQ STUFF (MUOVISOLA)

echo "rabbitmq provider..."
cd ../rabbitmq-provider/
./make release
cp rabbitmq-provider $DIR/
cp rabbitmq-provider-config.json $DIR/rabbitmq-provider-example-config.json

echo "rabbitmq-provider-haltian-message-handler..."
cd ../rabbitmq-provider-haltian-message-handler/
./make release
cp rabbitmq-provider-haltian-message-handler.so $DIR/

echo "rabbitmq-provider-premetec-message-handler..."
cd ../rabbitmq-provider-premetec-message-handler/
./make release
cp rabbitmq-provider-premetec-message-handler.so $DIR/

echo "rabbitmq-provider-advantech-message-handler..."
cd ../rabbitmq-provider-advantech-message-handler/
./make release
cp rabbitmq-provider-advantech-message-handler.so $DIR/

echo "rabbitmq-provider-pggate-message-handler..."
cd ../rabbitmq-provider-pggate-message-handler/
./make release
cp rabbitmq-provider-pggate-message-handler.so $DIR/

echo "rabbitmq-provider-begroup-message-handler..."
cd ../rabbitmq-provider-begroup-message-handler/
./make release
cp rabbitmq-provider-begroup-message-handler.so $DIR/

echo "rabbitmq-provider-hwgroup-message-handler..."
cd ../rabbitmq-provider-hwgroup-message-handler/
./make release
cp rabbitmq-provider-hwgroup-message-handler.so $DIR/

echo "se-azure-provider"
cd ../se-azure-provider/
./make release
cp ./build/se-azure-provider $DIR/

# WATERLEAK STUFF (MUOVISOLA)

echo "rabbitmq-provider-waterleak-message-handler..."
cd ../rabbitmq-provider-waterleak-message-handler/
./make release
cp rabbitmq-provider-waterleak-message-handler.so $DIR/

echo "waterleak-comms-provider..."
cd ../waterleak-comms-provider/
./make release
cp waterleak-comms-provider $DIR/
cp waterleak-comms-config-example.json $DIR/waterleak-comms-provider-example-config.json

# SRTP STUFF

echo "rabbitmq-provider-srtp-pressure-message-handler..."
cd ../rabbitmq-provider-srtp-pressure-message-handler/
./make release
cp rabbitmq-provider-srtp-pressure-message-handler.so $DIR/

echo "srtp-comms-provider..."
cd ../srtp-comms-provider/
./make release
cp srtp-comms-provider $DIR/
cp srtp-comms-config-example.json $DIR/waterleak-comms-provider-example-config.json

cd $BUILDS
tar -czf $1.tar.gz $1/
cd $HERE
echo "Archive: $BUILDS/$1.tar.gz"

