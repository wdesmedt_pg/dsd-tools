# DSD tooling

Personal playground for some dsdaemon tooling.

# package.sh

Script is a real WIP. It uses relative paths and has no configuration options.

Run this directly from the dsd-tools and pass the name for the package as the only argument;

It will compile (with `./make release`) and copy all relevant binaries and example configurations to the target folder and then create a tar.gz package with the same name.

`./package.sh v1.2` for example will create a folder `./v1.2/` and a file `./v1.2.tar.gz`.